resource "google_service_account" "api_deploy" {
  display_name = "Service Account for deploying deploy-api-service"
  account_id   = "deployment-api-sa"
}

resource "google_project_iam_member" "deploy_role_iam" {
  project = var.project_id

  role   = var.deploy_role
  member = "serviceAccount:${google_service_account.api_deploy.email}"
}

resource "google_service_account_iam_member" "deploy_service_user" {
  service_account_id = google_service_account.api_service_sa.name
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.api_deploy.email}"
}

resource "google_artifact_registry_repository_iam_member" "deploy_artifacts_iam" {
  provider = google-beta

  project    = google_artifact_registry_repository.api_repo.project
  location   = google_artifact_registry_repository.api_repo.location
  repository = google_artifact_registry_repository.api_repo.name

  role   = "roles/artifactregistry.writer"
  member = "serviceAccount:${google_service_account.api_deploy.email}"
}
