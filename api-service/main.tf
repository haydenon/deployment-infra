resource "google_artifact_registry_repository" "api_repo" {
  provider = google-beta

  location = var.region

  repository_id = "api"

  format = "DOCKER"
}

resource "google_service_account" "api_service_sa" {
  display_name = "Service Account for deploy-api-service"
  account_id   = "deploy-api-sa"
}

# resource "google_artifact_registry_repository_iam_member" "service_artifacts_iam" {
#   provider = google-beta

#   project    = google_artifact_registry_repository.api_repo.project
#   location   = google_artifact_registry_repository.api_repo.location
#   repository = google_artifact_registry_repository.api_repo.name

#   role   = "roles/artifactregistry.reader"
#   member = "serviceAccount:${google_service_account.api_service_sa.email}"
# }

resource "google_project_iam_member" "firestore_iam" {
  project = var.project_id

  role   = "roles/datastore.user"
  member = "serviceAccount:${google_service_account.api_service_sa.email}"
}

resource "google_cloud_run_service" "api_service" {
  location = var.region
  name     = "deploy-api-service"

  template {
    spec {
      service_account_name  = google_service_account.api_service_sa.email
      container_concurrency = 100
      timeout_seconds       = 15
      containers {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
        resources {
          limits = {
            cpu    = "1"
            memory = "512Mi"
          }
        }
      }
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/maxScale" = "2"
        "autoscaling.knative.dev/minScale" = "0"
      }
    }
  }

  lifecycle {
    ignore_changes = [
      template
    ]
  }
}

resource "google_cloud_run_service_iam_member" "api_service_member" {
  for_each = var.user_emails

  location = google_cloud_run_service.api_service.location
  project  = google_cloud_run_service.api_service.project
  service  = google_cloud_run_service.api_service.name

  role   = "roles/run.invoker"
  member = "user:${each.value}"
}
