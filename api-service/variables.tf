variable "region" {
  type = string
}

variable "deploy_role" {
  type = string
}

variable "project_id" {
  type = string
}

variable "user_emails" {
  type = set(string)
}
