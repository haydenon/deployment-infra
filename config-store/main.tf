locals {
  key_document_map = {
    for service_key, sa_key in var.deploy_sa_keys :
    service_key => {
      key = {
        stringValue = sa_key
      }
    }
  }
  service_config_document_map = {
    for service_key, service_details in var.service_config :
    service_key => {
      projectId = {
        stringValue = service_details.project_id
      }
      region = {
        stringValue = service_details.region
      }
      serviceName = {
        stringValue = service_details.service_name
      }
      serviceUrl = {
        stringValue = service_details.service_url
      }
      repository_base = {
        stringValue = service_details.repository_base
      }
      repository_name = {
        stringValue = service_details.repository_name
      }
    }
  }
}

resource "google_firestore_document" "key_document" {
  for_each    = var.deploy_sa_keys
  project     = var.project_id
  collection  = "deploySaKeys"
  document_id = "deployKey_${each.key}"
  fields      = jsonencode(local.key_document_map[each.key])
}

resource "google_firestore_document" "service_config_document" {
  for_each    = var.service_config
  project     = var.project_id
  collection  = "serviceConfig"
  document_id = "serviceConfig_${each.key}"
  fields      = jsonencode(local.service_config_document_map[each.key])
}
