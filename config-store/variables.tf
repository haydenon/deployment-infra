variable "project_id" {
  type = string
}

variable "deploy_sa_keys" {
  type = map(string)
}

variable "service_config" {
  type = map(object({
    project_id      = string
    region          = string
    service_name    = string
    service_url     = string
    repository_base = string
    repository_name = string
  }))
}
