resource "google_service_account" "service_sa" {
  display_name = "Service Account for ${local.service_name}"
  account_id   = "sa-${substr(local.service_suffix, 0, 24)}"
}

resource "google_cloud_run_service" "service" {
  location = var.region
  name     = local.service_name

  template {
    spec {
      service_account_name  = google_service_account.service_sa.email
      container_concurrency = 5
      timeout_seconds       = 10
      containers {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
        resources {
          limits = {
            cpu    = "1"
            memory = "128Mi"
          }
        }
      }
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/maxScale" = "1"
        "autoscaling.knative.dev/minScale" = "0"
      }
    }
  }

  lifecycle {
    ignore_changes = [template]
  }
}

resource "google_service_account_iam_member" "manual_service_user" {
  service_account_id = google_service_account.service_sa.name
  role               = "roles/iam.serviceAccountUser"
  member             = "user:${var.user_email}"
}

resource "google_cloud_run_service_iam_member" "member" {
  location = google_cloud_run_service.service.location
  project  = google_cloud_run_service.service.project
  service  = google_cloud_run_service.service.name
  role     = var.cloud_run_manual_role_name
  member   = "user:${var.user_email}"
}

resource "google_project_iam_member" "member" {
  project = var.project_id

  role   = var.cloud_run_projects_role_name
  member = "user:${var.user_email}"
}

resource "google_cloud_run_service_iam_member" "member_invoke" {
  location = google_cloud_run_service.service.location
  project  = google_cloud_run_service.service.project
  service  = google_cloud_run_service.service.name

  role   = "roles/run.invoker"
  member = "user:${var.user_email}"
}

# resource "google_artifact_registry_repository_iam_member" "service_example_artifacts_iam" {
#   provider = google-beta

#   project    = var.project_id
#   location   = var.region
#   repository = var.example_repo_name

#   role   = "roles/artifactregistry.reader"
#   member = "serviceAccount:${google_service_account.service_sa.email}"
# }
