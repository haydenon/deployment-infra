resource "google_service_account" "deploy" {
  display_name = "Service Account for deploying ${local.service_name}"
  account_id   = "deploy-sa-${local.service_suffix}"
}

resource "google_project_iam_member" "deploy_role_iam" {
  project = var.project_id

  role   = var.deploy_role
  member = "serviceAccount:${google_service_account.deploy.email}"
}

resource "google_service_account_iam_member" "deploy_service_user" {
  service_account_id = google_service_account.service_sa.name
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.deploy.email}"
}

resource "google_service_account_key" "deploy_key" {
  service_account_id = google_service_account.deploy.name

  keepers = {
    "version" = var.key_version
  }
}

resource "google_artifact_registry_repository_iam_member" "deploy_artifacts_iam" {
  provider = google-beta

  project    = google_artifact_registry_repository.deploy_service_repo.project
  location   = google_artifact_registry_repository.deploy_service_repo.location
  repository = google_artifact_registry_repository.deploy_service_repo.name

  role   = "roles/artifactregistry.writer"
  member = "serviceAccount:${google_service_account.deploy.email}"
}

resource "google_artifact_registry_repository_iam_member" "deploy_example_artifacts_iam" {
  provider = google-beta

  project    = var.project_id
  location   = var.region
  repository = var.example_repo_name

  role   = "roles/artifactregistry.reader"
  member = "serviceAccount:${google_service_account.service_sa.email}"
}
