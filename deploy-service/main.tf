locals {
  email_prefix = replace(var.user_email, "/@.*$/", "")
  service_suffix = replace(local.email_prefix, "/[^a-zA-Z0-9]/", "-")
  service_name = "deploy-service-${local.service_suffix}"
}
