output "deploy_sa_key_values" {
  value = base64decode(google_service_account_key.deploy_key.private_key)
}

output "service_details" {
  value = {
    project_id      = var.project_id
    region          = var.region
    service_name    = google_cloud_run_service.service.name
    service_url     = google_cloud_run_service.service.status.0.url
    repository_base = local.repository_base
    repository_name = local.repository_name
  }
}
