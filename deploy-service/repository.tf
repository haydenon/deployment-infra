resource "google_artifact_registry_repository" "deploy_service_repo" {
  provider = google-beta

  location = var.region

  repository_id = local.repository_name

  format = "DOCKER"
}

locals {
  repository_base = "${var.region}-docker.pkg.dev/${var.project_id}"
  repository_name = "repo-${local.service_suffix}"
}

resource "google_artifact_registry_repository_iam_member" "deploy_service_repo_iam" {
  provider = google-beta

  project    = google_artifact_registry_repository.deploy_service_repo.project
  location   = google_artifact_registry_repository.deploy_service_repo.location
  repository = google_artifact_registry_repository.deploy_service_repo.name

  role   = "roles/artifactregistry.reader"
  member = "user:${var.user_email}"
}

resource "google_artifact_registry_repository_iam_member" "example_repo_iam" {
  provider = google-beta

  project    = var.project_id
  location   = var.region
  repository = var.example_repo_name

  role   = "roles/artifactregistry.reader"
  member = "user:${var.user_email}"
}

