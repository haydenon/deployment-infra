variable "user_email" {
  type = string
}

variable "region" {
  type = string
}

variable "deploy_role" {
  type = string
}

variable "project_id" {
  type = string
}

variable "key_version" {
  type    = number
  default = 1
}

variable "cloud_run_manual_role_name" {
  type = string
}

variable "cloud_run_projects_role_name" {
  type = string
}

variable "example_repo_name" {
  type = string
}

