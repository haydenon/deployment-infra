resource "google_project_iam_custom_role" "deploy_role" {
  role_id     = "deployServiceRole"
  title       = "Service Deploy Role"
  description = "Deploy role for services"
  permissions = [
    "run.services.create",
    "run.services.update",
    "run.services.get",
    "run.revisions.list",
    "run.revisions.get"
  ]
}

module "deploy_services" {
  source = "./deploy-service"

  for_each = local.user_emails

  project_id                   = var.project_id
  deploy_role                  = "projects/${var.project_id}/roles/${google_project_iam_custom_role.deploy_role.role_id}"
  cloud_run_manual_role_name   = module.roles.cloud_run_manual_role_name
  cloud_run_projects_role_name = module.roles.cloud_run_projects_role_name
  user_email                   = each.value
  region                       = var.region
  example_repo_name            = module.example_repo.repo_name
}
