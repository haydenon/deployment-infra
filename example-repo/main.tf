resource "google_artifact_registry_repository" "example_repo" {
  provider = google-beta

  location = var.region

  repository_id = "example"

  format = "DOCKER"
}
