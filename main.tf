locals {
  infra_sa_email = "${var.infra_sa_user}@${var.project_id}.iam.gserviceaccount.com "
  user_emails    = toset(["hayden.oneill9@gmail.com"])
}

provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.zone
}

provider "google-beta" {
  project = var.project_id
  region  = var.region
  zone    = var.zone
}

terraform {
  backend "http" {
  }

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.77.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 3.77.0"
    }
  }
}
