resource "google_project_iam_custom_role" "cloud_run_manual_role" {
  role_id = "cloudRunManualRole"
  title   = "Cloud Run Manual Admin Role"
  permissions = [
    "run.services.get",
    "run.services.update",
    "run.services.getIamPolicy",
    "run.configurations.list",
    "run.locations.list",
    "run.routes.get",
    "run.routes.list"
  ]
}

resource "google_project_iam_custom_role" "cloud_run_projects_role" {
  role_id = "cloudRunProjectRole"
  title   = "Cloud Run Project Role"
  permissions = [
    "run.revisions.get",
    "run.revisions.list",
    "artifactregistry.repositories.get",
    "artifactregistry.repositories.list",
    "iam.serviceAccounts.list"
  ]
}
