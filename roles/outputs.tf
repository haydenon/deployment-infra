output "cloud_run_manual_role_name" {
  value = google_project_iam_custom_role.cloud_run_manual_role.name
}

output "cloud_run_projects_role_name" {
  value = google_project_iam_custom_role.cloud_run_projects_role.name
}
