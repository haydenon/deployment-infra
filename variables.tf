variable "project_id" {
  type = string
}

variable "region" {
  type    = string
  default = "australia-southeast1"
}

variable "zone" {
  type    = string
  default = "australia-southeast1-b"
}

variable "service_count" {
  type    = number
  default = 1
}

variable "infra_sa_user" {
  type = string
}
