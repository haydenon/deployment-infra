module "config_store" {
  source = "./config-store"

  project_id = var.project_id

  deploy_sa_keys = {
    for key in local.user_emails :
    key => module.deploy_services[key].deploy_sa_key_values
  }
  service_config = {
    for key in local.user_emails :
    key => module.deploy_services[key].service_details
  }
}

module "example_repo" {
  source = "./example-repo"

  region = var.region
}

module "roles" {
  source = "./roles"
}

module "api_service" {
  source = "./api-service"

  project_id  = var.project_id
  deploy_role = "projects/${var.project_id}/roles/${google_project_iam_custom_role.deploy_role.role_id}"
  region      = var.region

  user_emails = local.user_emails
}
